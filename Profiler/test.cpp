#define PROFILE_ON

#include <Windows.h>
#include "Profiler.h"

void Test1()
{
	Sleep(1000);
}

int main(void)
{
	PROFILE_INITIALIZE;

	PROFILE_BEGIN(L"Test1");
	Test1();
	PROFILE_END(L"Test1");
	PROFILE_OUT;
}