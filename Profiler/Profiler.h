#pragma once

/* ***************************************
	사용법)

	PROFILE_INITIALIZE;

	PROFILE_BEGIN("함수이름")
	함수();
	PROFILE_END("함수 이름")
	
	PROFILE_OUT;
**************************************** */

/* ***************************************
	전처리
**************************************** */

//#define PROFILE_ON
#ifdef PROFILE_ON
#define PROFILE_INITIALIZE		mylibrary::profiler::ProfileInitialize()
#define PROFILE_BEGIN(name)		mylibrary::profiler::ProfileBegin(name)
#define PROFILE_END(name)		mylibrary::profiler::ProfileEnd(name)
#define PROFILE_OUT				mylibrary::profiler::ProfileOutText()
#define PROFILE_ARR_MAX			(20)
#else
#define PROFILE_INITIALIZE 
#define PROFILE_BEGIN(name) 
#define PROFILE_END(name)
#define PROFILE_OUT 
#define PROFILE_ARR_MAX			(20)
#endif

namespace mylibrary
{
	namespace profiler
	{
		/* ***************************************
		구조체
		**************************************** */
		struct ProfileSlot
		{
			enum {
				NAME_LENGTH = 64,
			};

			wchar_t			_name[NAME_LENGTH];
			long long		_min1;
			long long		_min2;
			long long		_max1;
			long long		_max2;
			long long		_accumulated_call;
			long long		_accumulated_tick;
			long long		_start;
			unsigned long	_thread_ID;
			LONG			_b_used;
		};

		/* ***************************************
		함수
		**************************************** */
		void ProfileInitialize();
		void ProfileBegin(wchar_t* name);
		void ProfileEnd(wchar_t* name);
		void ProfileOutText();
	}
}