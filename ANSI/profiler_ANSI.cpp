//*************************************************
// #define PROFILE_ON을 해주면 작동 아니면 X
// PROFILE_BEGIN("함수이름")
// 함수();
// PROFILE_END("함수 이름")
// 처럼 사용.
//*************************************************

#include <stdio.h>
#include <tchar.h>
#include <time.h>
#include <windows.h>
#include "profiler.h"

struct ProfileSlot
{
	char _name[128];
	unsigned int _min1;
	unsigned int _min2;
	unsigned int _max1;
	unsigned int _max2;
	unsigned int _accumulated_call;
	unsigned int _accumulated_tick;
	unsigned int _start;
};

FILE* g_fp = nullptr;
ProfileSlot g_profile[PROFILE_ARR_MAX];

void ProfileInitialize()
{
	{
		for (unsigned int i = 0; i < PROFILE_ARR_MAX; i++)
		{
			g_profile[i]._name[0] = '\0';
			g_profile[i]._min1 = 0xFFFFFFFF;
			g_profile[i]._min2 = 0xFFFFFFFF;
			g_profile[i]._max1 = 0;
			g_profile[i]._max2 = 0;
			g_profile[i]._start = 0;
			g_profile[i]._accumulated_call = 0;
			g_profile[i]._accumulated_tick = 0;
		}
	}
}

void ProfileBegin(char* name)
{
	LARGE_INTEGER start;
	QueryPerformanceCounter(&start);

	for (unsigned int i = 0; i < PROFILE_ARR_MAX; i++)
	{
		if (g_profile[i]._name[0] == '\0')
		{
			strcpy_s(g_profile[i]._name, 128, name);
			g_profile[i]._start = start.QuadPart;

			break;
		}

		if (strcmp(g_profile[i]._name, name) == 0)
		{
			if (g_profile[i]._start != 0)
				throw 1;
			else
			{
				g_profile[i]._start = start.QuadPart;

				break;
			}
		}
	}

	if (GetAsyncKeyState(0x30) & 0x8001)
	{
		ProfileOutText();
	}
}

void ProfileEnd(char* name)
{
	LARGE_INTEGER end;
	QueryPerformanceCounter(&end);

	for (unsigned int i = 0; i < PROFILE_ARR_MAX; i++)
	{
		if (g_profile[i]._name[0] == '\0')
			throw 1;

		if (g_profile[i]._name[0] != '\0' && strcmp(g_profile[i]._name, name) == 0)
		{
			if (g_profile[i]._start == 0)
				throw 1;

			unsigned int accumulated = end.QuadPart - g_profile[i]._start;
			g_profile[i]._accumulated_tick += accumulated;
			g_profile[i]._accumulated_call += 1;
			g_profile[i]._start = 0;

			//******************************************
			// min
			//******************************************
			if (accumulated < g_profile[i]._min1)
			{
				g_profile[i]._min2 = g_profile[i]._min1;
				g_profile[i]._min1 = accumulated;
			}

			if (accumulated > g_profile[i]._min1 && accumulated < g_profile[i]._min2)
			{
				g_profile[i]._min2 = accumulated;
			}

			//******************************************
			// max
			//******************************************
			if (accumulated > g_profile[i]._max1)
			{
				g_profile[i]._max2 = g_profile[i]._max1;
				g_profile[i]._max1 = accumulated;
			}

			if (accumulated < g_profile[i]._max1 && accumulated > g_profile[i]._max2)
			{
				g_profile[i]._max2 = accumulated;
			}

			break;
		}
	}
}

void ProfileOutText()
{
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	float micro_time = (float)1000000 / freq.QuadPart;

	
	char file_name[128];
	struct tm t;
	time_t current;

	time(&current);
	localtime_s(&t, &current);

	sprintf_s(file_name, 128, "Profiling_%d%d%d_%d%d%d.txt", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);
	fopen_s(&g_fp, file_name, "w");

	if (g_fp == nullptr)
	{
		printf("ERROR : FILE OPEN FAILED \n");
		throw 1;
	}

	char title[128] = "           Name  |     Average  |        Min   |        Max   |      Call         |\n";
	fwrite(title, strlen(title) + 1, 1, g_fp);
	sprintf_s(title, "----------------------------------------------------------------------------------\n");
	fwrite(title, strlen(title) + 1, 1, g_fp);

	// min max 정리
	for (unsigned int i = 0; i < PROFILE_ARR_MAX; i++)
	{
		if (g_profile[i]._name[0] != '\0')
		{
			if (g_profile[i]._max2 == 0)
				g_profile[i]._max2 = g_profile[i]._max1;
			if (g_profile[i]._min2 == 0xFFFFFFFF)
				g_profile[i]._min2 = g_profile[i]._min1;
		}
		else
			break;
	}

	char format[128] = "%16s|%11.4f㎲ |%11.4f㎲ |%11.4f㎲ |%19u|\n";
	char out[128];

	for (unsigned int i = 0; i < PROFILE_ARR_MAX; i++)
	{
		if (g_profile[i]._name[0] != '\0')
		{
			unsigned int accumulated_tick;
			unsigned int accumulated_call;

			if (g_profile[i]._accumulated_call > 4)
			{
				accumulated_tick = g_profile[i]._accumulated_tick -
					g_profile[i]._max1 -
					g_profile[i]._max2 -
					g_profile[i]._min1 -
					g_profile[i]._min2;
				accumulated_call = g_profile[i]._accumulated_call - 4;
			}
			else
			{
				accumulated_tick = g_profile[i]._accumulated_tick;
				accumulated_call = g_profile[i]._accumulated_call;
			}

			sprintf_s(out, format, g_profile[i]._name, 
				(accumulated_tick * micro_time)/accumulated_call, 
				g_profile[i]._min2 * micro_time, 
				g_profile[i]._max2 * micro_time, 
				g_profile[i]._accumulated_call);

			fwrite(out, strlen(out) + 1, 1, g_fp);
		}
		else
			break;
	}

	fclose(g_fp);
}
