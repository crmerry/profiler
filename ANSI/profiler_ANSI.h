#pragma once

#define PROFILE_ON

#ifdef PROFILE_ON
#define PROFILE_INITIALIZE ProfileInitialize();
#define PROFILE_BEGIN(name) ProfileBegin(name);
#define PROFILE_END(name) ProfileEnd(name);
#define PROFILE_OUT ProfileOutText();
#define PROFILE_ARR_MAX 20
#else
#define PROFILE_INITIALIZE 
#define PROFILE_BEGIN(name) 
#define PROFILE_END(name)
#define PROFILE_OUT 
#define PROFILE_ARR_MAX 20
#endif

void ProfileInitialize();
void ProfileBegin(char* name);
void ProfileEnd(char* name);
void ProfileOutText();